# README

[![pipeline status](https://gitlab.com/justinvoelker/symfony-sample-bundle/badges/master/pipeline.svg)](https://gitlab.com/justinvoelker/symfony-sample-bundle/commits/master)
[![coverage report](https://gitlab.com/justinvoelker/symfony-sample-bundle/badges/master/coverage.svg)](https://gitlab.com/justinvoelker/symfony-sample-bundle/commits/master)

## Notes

* After including `"symfony/phpunit-bridge": "^4.3"` in composer.json, must execute `./vendor/bin/simple-phpunit` once to download phpunit.
* Do not exclude vendor/bin in PhpStorm. It is necessary for inclusion of PHPUnit.
