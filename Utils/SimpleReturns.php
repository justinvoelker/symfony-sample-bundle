<?php

namespace JustinVoelker\SymfonySampleBundle\Utils;

class SimpleReturns
{
    public function returnTrue(): bool
    {
        return true;
    }
}
