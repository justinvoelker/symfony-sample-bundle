<?php

namespace JustinVoelker\SymfonySampleBundle\Tests;

use JustinVoelker\SymfonySampleBundle\Utils\SimpleReturns;
use PHPUnit\Framework\TestCase;

class SimpleReturnsTest extends TestCase
{
    public function testBoolean(): void
    {
        $simpleReturns = new SimpleReturns();

        $this->assertTrue($simpleReturns->returnTrue());
    }
}
